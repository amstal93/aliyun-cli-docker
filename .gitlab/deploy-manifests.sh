#!/usr/bin/env bash

# Stop on any non-zero return codes.
set -e

# Get the current location of this script
CURRENT_DIR="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

# Import shared functions
source "${CURRENT_DIR}/common.sh"

github_versions=($(get_versions_from_github))
docker_hub_versions=$(get_versions_from_docker_hub)

# Get the latest version
latest_version=${github_versions[0]/v/}

# Reverse the order of the github versions
reverse_github_versions_string=""
for github_version in ${github_versions[@]}; do
  reverse_github_versions_string="${github_version} ${reverse_github_versions_string}"
done
reverse_github_versions=(${reverse_github_versions_string})

# Loop through the github versions
for github_version in ${reverse_github_versions[@]}; do
  # Build the manifests for the versions every time
  (
    # Allow non-zero exit codes within this sub-shell
    set +e

    docker_hub_version=${github_version/v/}

    # Check if the docker hub version already exists
    echo ${docker_hub_versions} | grep -q " ${docker_hub_version} "
    result=$?

    if [ $result = 1 ] || [ "${FORCE_MANIFEST_RECREATION}" = "true" ]; then
      build_manifest "${docker_hub_version}"
    fi
  )
done

# Set the latest version
set_as_latest_version ${latest_version}