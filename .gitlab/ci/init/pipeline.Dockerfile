ARG ARCH=amd64

FROM ${ARCH}/docker:19.03.8

RUN apk update --no-cache && apk add --no-cache jq bash curl

CMD [ "/bin/bash" ]