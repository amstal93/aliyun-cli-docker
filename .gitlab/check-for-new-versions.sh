#!/bin/bash

# Stop on any non-zero return codes.
set -e

# Get the current location of this script
CURRENT_DIR="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

# Import shared functions
source "${CURRENT_DIR}/common.sh"

github_versions=($(get_versions_from_github))
docker_hub_versions=$(get_versions_from_docker_hub)

# Loop through the github versions
for github_version in ${github_versions[@]}; do
  # Find the versions that are not on docker hub
  (
    # Within this subshell, allow non-zero exit codes
    set +e

    docker_hub_version=${github_version/v/}

    # Check if version exists
    echo "Checking ${docker_hub_version}..."
    echo ${docker_hub_versions} | grep -q ${docker_hub_version}
    res=$?

    # If the version doesn't exist, trigger the build jobs
    if [ ${res} -ne 0 ]; then
        trigger_build_jobs

        # Exit with a non-zero exit code to stop checking versions
        exit 1
    fi
  ) || exit 0
done